pkill python
git reset --hard
git checkout master
git pull --force
killall gunicorn
npm i
pipenv install
pipenv run python manage.py collectstatic --noinput --clear
pipenv run python manage.py migrate --noinput
pipenv run gunicorn wallofshame.wsgi --daemon --log-level DEBUG
nginx -s reload
nohup pipenv run python manage.py runapscheduler &
