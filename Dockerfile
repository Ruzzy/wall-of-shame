# syntax=docker/dockerfile:1
FROM python:3.8-slim
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
# Downloads directory because collectstatic expects it
RUN mkdir /downloads
RUN mkdir /wallofshamestatic
WORKDIR /code
COPY . /code/
RUN chmod +x /code/run.sh
RUN apt-get update
RUN apt-get -y install libpq-dev gcc npm
RUN pip install pipenv
RUN pipenv install --system
RUN npm install
RUN python manage.py collectstatic --noinput
