# A shared TODO list based on Django and Bootstrap

## Must install the following
### npm: 
`choco install nodejs`

`npm i` in project directory

## pipenv:
Ensure you uninstall any currently installed virtualenv installs first

`pip install pipenv`

`pipenv install`
to install the project packages

`pipenv shell`
to activate environment
