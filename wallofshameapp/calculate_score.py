from datetime import date

from wallofshame.settings import DONE_STRING, ABANDONED_STRING

OVERDUE_PENALTY_MULTIPLIER = 0.9


def calculate_score(task_list):
    number_of_tasks_done = 0
    number_of_tasks_not_done = 0
    number_of_tasks_overdue = 0

    for task in task_list:
        if task.status.name == DONE_STRING:
            number_of_tasks_done += 1
        elif task.status.name == ABANDONED_STRING:
            pass
        elif task.due > date.today():
            number_of_tasks_not_done += 1
        else:
            number_of_tasks_not_done += 1
            number_of_tasks_overdue += 1

    # Score is the percentage of tasks completed, with a further multiplying penalty imposed if a task is overdue
    try:
        score = round(100 * (number_of_tasks_done / (number_of_tasks_not_done + number_of_tasks_done)) * (
                OVERDUE_PENALTY_MULTIPLIER ** number_of_tasks_overdue))
    except:
        score = 0
    return score
