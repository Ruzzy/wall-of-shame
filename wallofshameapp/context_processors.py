from wallofshame.settings import SITE_NAME
from wallofshameapp.models import List


def add_variable_to_context(request):
    return {
        "lists": List.objects.all(),
        "site_name": SITE_NAME
    }
