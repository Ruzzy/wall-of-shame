from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout
from django import forms

from wallofshame.settings import NOT_DONE_STRING
from wallofshameapp.models import Task, List, Status, Group, Profile


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['list', 'status', 'name', 'due']
        labels = {
            "list": "Group"
        }
        widgets = {
            'due': forms.DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Task Name"
        self.fields['list'].queryset = List.objects.filter(user=user)
        self.fields['list'].label_from_instance = lambda obj: "%s" % obj.group.name
        self.fields['list'].required = True
        self.fields['status'].required = True
        self.fields['status'].initial = Status.objects.get(name=NOT_DONE_STRING)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'list',
            'due',
            'status',
            Submit('submit', 'Submit')
        )


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name']
        labels = {
            "name": "Group Name"
        }

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            Submit('submit', 'Submit')
        )


class GroupTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['list', 'status', 'name', 'due']
        labels = {
            "list": "Group"
        }
        widgets = {
            'due': forms.DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super(GroupTaskForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Task Name"
        self.fields['status'].required = True
        self.fields['status'].initial = Status.objects.get(name=NOT_DONE_STRING)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'due',
            'status',
            Submit('submit', 'Submit')
        )


class GroupUserForm(forms.Form):
    username = forms.CharField(label='Username')

    def __init__(self, *args, **kwargs):
        super(GroupUserForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'username',
            Submit('submit', 'Submit')
        )


class TaskUpdateForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['status', 'name', 'due']
        widgets = {
            'due': forms.DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super(TaskUpdateForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Task Name"
        self.fields['status'].required = True

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'due',
            'status',
            Submit('submit', 'Submit')
        )


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['display_name', 'timezone', 'notifications_enabled', 'notification_time', 'focused',
                  'max_focused_tasks']
        widgets = {
            'notification_time': forms.TimeInput(attrs={'type': 'time'})
        }

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            'display_name',
            'timezone',
            'notifications_enabled',
            'notification_time',
            'focused',
            'max_focused_tasks',
            Submit('submit', 'Submit')
        )
