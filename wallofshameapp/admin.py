from django.contrib import admin

from wallofshameapp.models import Task, List, Group, Status, Profile, Shame


class SoftDeletionAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = self.model.all_objects
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def delete_model(self, request, obj):
        obj.hard_delete()


class ShameAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)


admin.site.register(Shame, ShameAdmin)
admin.site.register(Profile)
admin.site.register(List, SoftDeletionAdmin)
admin.site.register(Task, SoftDeletionAdmin)
admin.site.register(Group)
admin.site.register(Status)
