import datetime
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import EmailMessage
from django.db import IntegrityError
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, DeleteView, UpdateView, FormView
from fcm_django.models import FCMDevice

from wallofshame.settings import CSRF_VERIFICATION_FAILED_MESSAGE, NOT_MEMBER_MESSAGE, NOT_ADMIN_MESSAGE, \
    PROFILE_UPDATED_MESSAGE, NOT_DONE_STRING
from wallofshameapp.calculate_score import calculate_score
from wallofshameapp.forms import TaskForm, TaskUpdateForm, GroupTaskForm, GroupUserForm, GroupForm, ProfileForm
from wallofshameapp.models import Group, Task, List, Shame
from wallofshameapp.utils import has_devices, time_until_next_shame


class MyTasksView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if len(request.user.list_set.all()) == 0:
            group_name = "{}'s Personal Tasks".format(request.user)
            new_group = Group(name=group_name, personal_group=True)
            new_group.save()
            new_list = List(user=self.request.user, group=new_group, admin=True)
            new_list.save()

        lists = request.user.list_set.all()

        context = {"lists": lists}
        return render(request, 'wallofshameapp/my_tasks.html', context)


class GroupTasksView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        group = Group.objects.get(pk=pk)
        user_scores = {}

        user_list = List.objects.get(user=request.user, group=group)
        is_admin = user_list.admin

        for list in group.list_set.all():
            user_scores[str(list.user)] = calculate_score(list.task_set.all())
        user_ranks = {k: rank + 1 for rank, (k, v) in
                      enumerate(sorted(user_scores.items(), key=lambda item: item[1], reverse=True))}

        for list in group.list_set.all():
            list.rank = user_ranks[str(list.user)]
            list.save()

        context = {"group": group, "user_scores": user_scores, "user_ranks": user_ranks, "is_admin": is_admin}
        return render(request, 'wallofshameapp/group_tasks.html', context)


class MyTaskCreate(LoginRequiredMixin, CreateView):
    form_class = TaskForm
    template_name = 'wallofshameapp/task_form.html'

    def get_form_kwargs(self):
        kwargs = super(MyTaskCreate, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse('my-tasks')


class MakeAdmin(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        user = self.kwargs.get('user')
        group = self.kwargs.get('group')
        request_user = self.request.user
        try:
            request_user_list = List.objects.get(group=group, user=request_user)
        except List.DoesNotExist:
            messages.error(self.request, NOT_MEMBER_MESSAGE)
            return HttpResponseRedirect(reverse('my-tasks'))
            return reverse('my-tasks')
        if not request_user_list.admin:
            messages.error(self.request, NOT_ADMIN_MESSAGE)
            return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": group}))

        user_list = List.objects.get(user=user, group=group)
        user_list.admin = True
        user_list.save()
        return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": group}))


class RenounceAdmin(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        group = self.kwargs.get('group')

        user_list = List.objects.get(user=request.user, group=group)
        user_list.admin = False
        user_list.save()
        return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": group}))


class LeaveGroup(LoginRequiredMixin, DeleteView):
    def get_object(self, queryset=None):
        group = self.kwargs.get('pk')
        return List.objects.get(user=self.request.user, group=group)

    def get_success_url(self):
        return reverse('my-tasks')


class ToggleFocused(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        profile = self.request.user.profile
        profile.focused = not profile.focused
        profile.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


class GroupCreate(LoginRequiredMixin, CreateView):
    form_class = GroupForm
    template_name = 'wallofshameapp/create_group_form.html'

    def form_valid(self, form):
        self.object = form.save()
        new_list = List(user=self.request.user, group=self.object, admin=True)
        new_list.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('group-tasks', args=(self.object.pk,))


@method_decorator(csrf_exempt, name='dispatch')
class DeviceCreate(View):
    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        token = request.POST.get('token')

        user = authenticate(username=username, password=password)
        if user is not None:
            if not any(device.registration_id == token for device in FCMDevice.objects.all()):
                device = FCMDevice()
                device.user = user
                device.registration_id = token
                device.type = "Android"
                device.save()
            return HttpResponse()
        else:
            return HttpResponse(status=401)


class GroupTaskCreate(LoginRequiredMixin, CreateView):
    form_class = GroupTaskForm
    template_name = 'wallofshameapp/group_task_form.html'

    def get_context_data(self, **kwargs):
        context = super(GroupTaskCreate, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        context['group'] = Group.objects.get(pk=pk)
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        pk = self.kwargs.get('pk')
        group = Group.objects.get(pk=pk)
        user = self.request.user
        obj.list = List.objects.get(group=group, user=user)
        obj.save()
        return super(GroupTaskCreate, self).form_valid(form)

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse('group-tasks', kwargs={"pk": pk})


class GroupUserAdd(LoginRequiredMixin, FormView):
    form_class = GroupUserForm
    template_name = 'wallofshameapp/group_user_form.html'

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get('pk')
        try:
            request_user_list = List.objects.get(group=pk, user=self.request.user)
        except List.DoesNotExist:
            messages.error(self.request, NOT_MEMBER_MESSAGE)
            return HttpResponseRedirect(reverse('my-tasks'))
        if not request_user_list.admin:
            messages.error(self.request, NOT_ADMIN_MESSAGE)
            return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": pk}))
        return super(GroupUserAdd, self).get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GroupUserAdd, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        context['group'] = Group.objects.get(pk=pk)
        return context

    def form_valid(self, form):
        pk = self.kwargs.get('pk')
        group = Group.objects.get(pk=pk)
        request_user = self.request.user
        try:
            request_user_list = List.objects.get(group=group, user=request_user)
        except List.DoesNotExist:
            messages.error(self.request, NOT_MEMBER_MESSAGE)
            return HttpResponseRedirect(reverse('my-tasks'))
        if not request_user_list.admin:
            messages.error(self.request, NOT_ADMIN_MESSAGE)
            return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": group.pk}))

        username = form.cleaned_data['username']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            messages.error(self.request, "User " + username + " cannot be found")
            return super(GroupUserAdd, self).form_invalid(form)

        try:
            obj = List(user=user, group=group)
            obj.save()
            messages.success(self.request, username + " has been added to " + str(group))
        except IntegrityError as e:
            if 'UNIQUE' in e.args[0]:
                messages.info(self.request, username + " is already in " + str(group))
            else:
                messages.error(self.request, "Something went wrong")
                return super(GroupUserAdd, self).form_invalid(form)
        return super(GroupUserAdd, self).form_valid(form)

    def get_success_url(self):
        pk = self.kwargs.get('pk')
        return reverse('group-tasks', kwargs={"pk": pk})


class TaskDelete(LoginRequiredMixin, DeleteView):
    model = Task

    def get_success_url(self):
        next = self.request.GET.get('next')
        if next:
            return next
        else:
            return reverse('my-tasks')


class TaskUpdate(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = TaskUpdateForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        next = self.request.GET.get('next')
        if next:
            return next
        else:
            return reverse('my-tasks')


class CustomPasswordChangeView(LoginRequiredMixin, PasswordChangeView):
    template_name = 'account/password_change.html'

    def get_success_url(self):
        return reverse('my-tasks')


class ProfileUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    form_class = ProfileForm
    success_message = PROFILE_UPDATED_MESSAGE

    def get_object(self, queryset=None):
        return self.request.user.profile

    def get_success_url(self):
        return reverse('profile-update')


class TaskShame(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        shamer = self.request.user
        task = get_object_or_404(Task, pk=self.kwargs.get('pk'))
        group = task.list.group
        shamee = task.list.user

        try:
            List.objects.get(group=group, user=shamer)
        except List.DoesNotExist:
            messages.error(self.request, NOT_MEMBER_MESSAGE)
            return HttpResponseRedirect(reverse('my-tasks'))
        if task.status.name != NOT_DONE_STRING:
            messages.warning(self.request, str(shamee) + " cannot be shamed for " + task.name)
            return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": group.pk}))

        time_till_next_shame = time_until_next_shame(task, shamer)
        if time_till_next_shame and time_till_next_shame > datetime.timedelta(0):
            messages.warning(self.request, str(shamee) + " cannot be shamed for " + str(
                task) + " for {} hours and {} minutes".format(time_till_next_shame.seconds // 3600,
                                                              time_till_next_shame.seconds // 60 % 60))
        else:
            shame = Shame(shamer=shamer, task=task)
            shame.save()

            subject = "You have been shamed on GoalMates!"
            body = render_to_string("wallofshameapp/shame.txt",
                                    {"shamer": shamer.profile.display_name, "task": task})
            link = request.build_absolute_uri(reverse('group-tasks', args=[task.list.group.pk]))

            if has_devices(task):
                devices = FCMDevice.objects.filter(user=shamee)

                for device in devices:
                    device.send_message(subject, body)

            message = EmailMessage(
                subject=subject,
                to=[shamee.email]
            )
            message.template_id = 'shame'
            message.merge_data = {
                shamee.email: {'subject': subject, 'body': body, 'link': link}}
            message.send()

            messages.success(self.request, str(shamee.profile.display_name) + " shamed for " + task.name)

        return HttpResponseRedirect(reverse('group-tasks', kwargs={"pk": group.pk}))


def csrf_failure(request, reason=""):
    messages.warning(request, CSRF_VERIFICATION_FAILED_MESSAGE)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
