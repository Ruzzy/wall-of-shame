from django.utils import timezone
from fcm_django.models import FCMDevice

from wallofshame import settings
from wallofshameapp.models import Shame


def has_devices(task):
    return FCMDevice.objects.filter(user=task.list.user).exists()


def time_until_next_shame(task, user):
    shame_threshold = timezone.now() - settings.SHAME_TIMEOUT
    last_shame = Shame.objects.filter(shamer=user, task=task).last()

    if last_shame:
        return last_shame.created - shame_threshold
    else:
        return None
