import logging

import pytz
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.utils import timezone
from django_apscheduler.jobstores import DjangoJobStore
from fcm_django.models import FCMDevice

from wallofshame.settings import NOT_DONE_STRING, DEFAULT_FROM_EMAIL
from wallofshameapp.models import Profile

from django.core.mail import send_mail, EmailMessage

logger = logging.getLogger(__name__)


def send_users_overdue_tasks():
    now_utc = timezone.now()
    profiles_to_notify = []
    for profile in Profile.objects.all():
        now_user = now_utc.astimezone(profile.timezone if profile.timezone else pytz.timezone(settings.TIME_ZONE))
        if profile.notifications_enabled and profile.notification_time is not None and profile.notification_time.hour == now_user.hour and profile.notification_time.minute == now_user.minute:
            profiles_to_notify.append(profile)

    friend_overdue_tasks_notifications = {}
    for profile in profiles_to_notify:
        # Send nudges about who can be shamed
        notification_string = ""
        for li in profile.user.list_set.all():
            shameable_users_in_group = set()

            for li2 in li.group.list_set.all():
                foreign_user = li2.user
                if foreign_user == profile.user:
                    continue
                for t in li2.task_set.all():
                    now_user = now_utc.astimezone(
                        foreign_user.profile.timezone if foreign_user.profile.timezone else pytz.timezone(
                            settings.TIME_ZONE))
                    if t.due < now_user.date() and t.status.name == NOT_DONE_STRING:
                        shameable_users_in_group.add(foreign_user.profile.display_name)
            if len(shameable_users_in_group) == 1:
                notification_string = notification_string + list(shameable_users_in_group)[
                    0] + " in group " + li.group.name + " can be shamed.<br>"
            elif shameable_users_in_group:
                notification_string = notification_string + ", ".join(
                    list(shameable_users_in_group)[:-1])
                notification_string = notification_string + " and " + list(shameable_users_in_group)[
                    -1] + " in group " + li.group.name + " can be shamed.<br>"
        if notification_string != "":
            friend_overdue_tasks_notifications[profile.display_name] = notification_string

        # Send email notifications about overdue tasks
        lists = profile.user.list_set.all()
        tasks_to_notify = []
        for li in lists:
            for t in li.task_set.all():
                now_user = now_utc.astimezone(
                    profile.timezone if profile.timezone else pytz.timezone(settings.TIME_ZONE))
                if t.due <= now_user.date() and t.status.name == NOT_DONE_STRING:
                    tasks_to_notify.append(t.name)

        if tasks_to_notify or profile.display_name in friend_overdue_tasks_notifications:
            body = ""
            if tasks_to_notify:
                length = len(tasks_to_notify)
                subject = "You have overdue tasks on Goalmates!"
                body = ("1 task is due:<br><br>" if length == 1 else str(
                    length) + " tasks are due:<br><br>") + render_to_string(
                    "wallofshameapp/overdue_tasks_notification.txt", {"tasks": tasks_to_notify}).replace("\n",
                                                                                                         "<br>") + "<br><br>"
            if profile.display_name in friend_overdue_tasks_notifications:
                body = body + "Your friends have overdue tasks.<br><br>" + \
                       friend_overdue_tasks_notifications[
                           profile.display_name] + "<br><br>Shame away!"
                if not tasks_to_notify:
                    subject = "Shame your friends for overdue tasks on GoalMates!"
            link = Site.objects.get_current().domain
            message = EmailMessage(
                subject=subject,
                to=[profile.user.email]
            )
            message.template_id = 'shame'
            message.merge_data = {
                profile.user.email: {'subject': subject, 'body': body, 'link': link}}
            message.send()

    unique_devices = []
    for device in FCMDevice.objects.all():
        if not any(i.registration_id == device.registration_id for i in unique_devices):
            unique_devices.append(device)

    # Send notifications about overdue tasks to those users who have a device
    for device in unique_devices:
        if device.user.profile is None or device.user.profile not in profiles_to_notify:
            continue
        lists = device.user.list_set.all()
        tasks_to_notify = []
        for li in lists:
            for t in li.task_set.all():
                now_user = now_utc.astimezone(
                    device.user.profile.timezone if device.user.profile.timezone else pytz.timezone(settings.TIME_ZONE))
                if t.due <= now_user.date() and t.status.name == NOT_DONE_STRING:
                    tasks_to_notify.append(t.name)

        if tasks_to_notify:
            length = len(tasks_to_notify)
            title = "1 task is due" if length == 1 else str(length) + " tasks are due"
            body = render_to_string("wallofshameapp/overdue_tasks_notification.txt", {"tasks": tasks_to_notify})
            device.send_message(title, body)

        if device.user.profile.display_name in friend_overdue_tasks_notifications:
            nudge_title = "Your friends have overdue tasks."
            device.send_message(nudge_title,
                                friend_overdue_tasks_notifications[device.user.profile.display_name].replace("<br>",
                                                                                                             "\n"))

    logger.debug("Sending notifications at " + str(now_utc) + " for " + str(len(profiles_to_notify)) + " profiles")


class Command(BaseCommand):
    help = "Runs apscheduler."

    def handle(self, *args, **options):
        scheduler = BlockingScheduler(timezone=settings.TIME_ZONE)
        scheduler.add_jobstore(DjangoJobStore(), "default")

        scheduler.add_job(
            send_users_overdue_tasks,
            trigger=CronTrigger(minute="*"),
            id="my_job",
            max_instances=1,
            replace_existing=True,
        )

        try:
            logger.info("Starting scheduler...")
            scheduler.start()
        except KeyboardInterrupt:
            logger.info("Stopping scheduler...")
            scheduler.shutdown()
            logger.info("Scheduler shut down successfully!")
