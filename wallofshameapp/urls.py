from django.urls import path

from wallofshameapp.views import MyTasksView, GroupTasksView, MyTaskCreate, TaskDelete, TaskUpdate, GroupTaskCreate, \
    GroupUserAdd, GroupCreate, MakeAdmin, RenounceAdmin, LeaveGroup, ToggleFocused, DeviceCreate, TaskShame

urlpatterns = [
    path('<int:pk>/', GroupTasksView.as_view(), name='group-tasks'),
    path('<int:pk>/task/add/', GroupTaskCreate.as_view(), name='group-add-task'),
    path('<int:pk>/user/add/', GroupUserAdd.as_view(), name='group-add-user'),
    path('focused/', ToggleFocused.as_view(), name='toggle-focused'),
    path('group/add/', GroupCreate.as_view(), name='group-create'),
    path('group/<int:group>/user/<int:user>/admin/', MakeAdmin.as_view(), name='make-admin'),
    path('group/<int:group>/renounce-admin/', RenounceAdmin.as_view(), name='group-renounce-admin'),
    path('group/<int:pk>/leave/', LeaveGroup.as_view(), name='leave-group'),
    path('task/add/', MyTaskCreate.as_view(), name='add'),
    path('task/<int:pk>/delete/', TaskDelete.as_view(), name='task-delete'),
    path('task/<int:pk>/update/', TaskUpdate.as_view(), name='task-update'),
    path('task/<int:pk>/shame/', TaskShame.as_view(), name='task-shame'),
    path('device/add/', DeviceCreate.as_view(), name='device-create'),
    path('', MyTasksView.as_view(), name='my-tasks'),
]
