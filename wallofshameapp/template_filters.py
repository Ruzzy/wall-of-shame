import datetime

from django.template.defaulttags import register

from wallofshame import settings
from wallofshameapp.utils import time_until_next_shame


@register.filter
def get_item(dictionary, key):
    return dictionary.get(str(key))


@register.filter
def status_icon_class(task):
    name = task.status.name
    if task.is_due():
        return "fas fa-exclamation text-danger"
    elif name == settings.DONE_STRING:
        return "far fa-check-square text-success"
    elif name == settings.ABANDONED_STRING:
        return "far fa-minus-square text-secondary"
    else:
        return "far fa-square text-warning"


@register.filter
def newest_tasks(task_list, max_tasks=20):
    max_tasks = int(max_tasks)
    task_list = list(task_list)
    task_list.sort(key=lambda x: x.due, reverse=False)

    if len(task_list) > max_tasks:
        task_list = task_list[-max_tasks:]

    return task_list


@register.filter
def most_urgent_tasks(task_list, max_tasks=5):
    max_tasks = int(max_tasks)
    task_list = list(task_list)
    task_list.sort(key=lambda x: x.due, reverse=False)
    not_done_tasks = [task for task in task_list if task.status.name == settings.NOT_DONE_STRING]
    not_done_tasks = not_done_tasks[:max_tasks]

    return not_done_tasks


@register.filter
def is_shameful(task, user):
    time_till_next_shame = time_until_next_shame(task, user)
    return task.overdue() and (
            not time_till_next_shame or time_till_next_shame < datetime.timedelta(0))
