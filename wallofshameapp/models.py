import datetime

import pytz
from django.contrib.auth.models import User
from django.db import models
from django.db.models import QuerySet
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from timezone_field import TimeZoneField

from wallofshame import settings

MAX_STRING_LENGTH = 100
DEFAULT_MAX_FOCUSED_TASKS = 3


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    timezone = TimeZoneField(blank=True, null=True)
    notification_time = models.TimeField(default='12:00')
    notifications_enabled = models.BooleanField(default=True)
    focused = models.BooleanField(default=False)
    max_focused_tasks = models.PositiveIntegerField(default=DEFAULT_MAX_FOCUSED_TASKS)
    display_name = models.CharField(blank=True, null=True, max_length=MAX_STRING_LENGTH)

    def __str__(self):
        return self.display_name + "'s Profile"

    def save(self, *args, **kwargs):
        if not self.display_name:
            self.display_name = self.user.username
        super(Profile, self).save(*args, **kwargs)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Group(models.Model):
    name = models.CharField(max_length=MAX_STRING_LENGTH)
    personal_group = models.BooleanField(default=False)

    def __str__(self):
        if self.personal_group:
            return 'Personal Tasks'
        return self.name


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super(SoftDeletionManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class SoftDeletionQuerySet(QuerySet):
    def delete(self):
        return super(SoftDeletionQuerySet, self).update(deleted_at=timezone.now())

    def hard_delete(self):
        return super(SoftDeletionQuerySet, self).delete()

    def alive(self):
        return self.filter(deleted_at=None)

    def dead(self):
        return self.exclude(deleted_at=None)


class SoftDeletionModel(models.Model):
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()

    def hard_delete(self):
        super(SoftDeletionModel, self).delete()


class List(SoftDeletionModel):
    class Meta:
        ordering = ['rank']
        unique_together = ('user', 'group')

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)
    rank = models.IntegerField(default=0)
    admin = models.BooleanField(default=False)

    def __str__(self):
        return self.group.name + ": " + self.user.username


class Status(models.Model):
    name = models.CharField(max_length=MAX_STRING_LENGTH)

    def __str__(self):
        return self.name


class Task(SoftDeletionModel):
    class Meta:
        ordering = ['due']

    list = models.ForeignKey(List, on_delete=models.CASCADE, blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=MAX_STRING_LENGTH)
    due = models.DateField()

    def overdue(self):
        now_utc = timezone.now()
        user_timezone = self.list.user.profile.timezone
        now_user = now_utc.astimezone(
            user_timezone if user_timezone else pytz.timezone(settings.TIME_ZONE))
        return self.status.name == settings.NOT_DONE_STRING and self.due < now_user.date()

    def is_due(self):
        now_utc = timezone.now()
        user_timezone = self.list.user.profile.timezone
        now_user = now_utc.astimezone(
            user_timezone if user_timezone else pytz.timezone(settings.TIME_ZONE))
        return self.status.name == settings.NOT_DONE_STRING and self.due <= now_user.date()

    def __str__(self):
        return self.name


class Shame(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    shamer = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.shamer) + " shamed " + str(self.task.list.user) + " for " + str(self.task)
