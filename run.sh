#!/bin/sh
python manage.py migrate
if [ "$DJANGO_DEBUG" != "False" ]; then
	echo "Creating Django superuser"
	DJANGO_SUPERUSER_PASSWORD=password python manage.py createsuperuser --noinput --username=admin --email=a@b.com
fi
gunicorn wallofshame.wsgi:application --bind 0.0.0.0:8000 --access-logfile '-' --log-level 'debug'
