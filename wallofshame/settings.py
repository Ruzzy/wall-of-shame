import logging
import os
from datetime import timedelta
from pathlib import Path

from django.contrib import messages

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = '&8=-%0em$6qeznlkh!e3k!cs$e0-@hj_zy(a#)y1a6$t!wlsqo'

DEBUG = True
if 'DJANGO_DEBUG' in os.environ:
    DEBUG = os.environ['DJANGO_DEBUG'] != 'False'

if os.name == 'nt' and not DEBUG:
    print(
        "Running debug=false on Windows, aborting. Override manually in settings.py if you are sure you would like to do this")
    exit()

ALLOWED_HOSTS = ['*']

ANYMAIL = {
    "MAILGUN_API_KEY": "dfab463334b471b9339f12eafe64597e-29561299-7e4ef290",
    "MAILGUN_SENDER_DOMAIN": "mg.goalmates.live"
}

EMAIL_BACKEND = 'anymail.backends.mailgun.EmailBackend'
DEFAULT_FROM_EMAIL = "noreply@goalmates.live"
SERVER_EMAIL = DEFAULT_FROM_EMAIL

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend"
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'wallofshameapp',
    'timezone_field',
    'crispy_forms',

    'allauth',
    'allauth.account',
    'allauth.socialaccount',

    "fcm_django",
    "django_apscheduler",

    "anymail"
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'wallofshame.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'wallofshameapp.context_processors.add_variable_to_context',
            ],
            'libraries': {
                'template_filters': 'wallofshameapp.template_filters',
            }
        },
    },
]

WSGI_APPLICATION = 'wallofshame.wsgi.application'

if DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': BASE_DIR / 'db.sqlite3'
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'goalmates',
            'USER': 'goalmates',
            'PASSWORD': 'sbzRur4L3VNV8xVj3pzz',
            'HOST': 'goalmates.cplkv9ilt42d.us-east-1.rds.amazonaws.com',
            'PORT': '5432',
        }
    }
logger = logging.getLogger(__name__)
logger.warning(f"DEBUG = {DEBUG}")
logger.warning(f"DATABASES default ENGINE = {DATABASES['default']['ENGINE']}")

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Australia/Melbourne'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = "/wallofshamestatic/"

CRISPY_TEMPLATE_PACK = 'bootstrap4'

STATICFILES_DIRS = ('node_modules', '/downloads')
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

SITE_ID = 1
SITE_NAME = "GoalMates"

LOGIN_REDIRECT_URL = "/"

DONE_STRING = "Done"
NOT_DONE_STRING = "Not Done"
ABANDONED_STRING = "Abandoned"

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_AUTHENTICATION_METHOD = "username_email"

FCM_DJANGO_SETTINGS = {
    "APP_VERBOSE_NAME": "goalmates-4efd6",
    "FCM_SERVER_KEY": "AAAADS1ncbs:APA91bFz1hW4ZlVzAR-hGmVJuMEIpec99L5bGWhJxfbiw1wNDb1mLSevlrTyec0IeHPRRhL0AGoIzoenCRqDukXBmyvdQwi01FCpr4bndnPP0oV3iuD2c68YTCaCyRECyN_04A4wAigZ",
    "ONE_DEVICE_PER_USER": False,
    "DELETE_INACTIVE_DEVICES": True,
}

MESSAGE_TAGS = {
    messages.ERROR: 'danger',
}

SHAME_TIMEOUT = timedelta(hours=24)

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

CSRF_VERIFICATION_FAILED_MESSAGE = "Something went wrong, please try again"
NOT_MEMBER_MESSAGE = "You are not a member of this group"
NOT_ADMIN_MESSAGE = "You are not an admin of this group"
PROFILE_UPDATED_MESSAGE = "Profile updated"

CSRF_FAILURE_VIEW = "wallofshameapp.views.csrf_failure"
