from django.contrib import admin
from django.urls import include, path

from wallofshameapp.views import CustomPasswordChangeView, ProfileUpdate

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/password/change/', CustomPasswordChangeView.as_view(), name="account_password_change"),
    path('accounts/profile/', ProfileUpdate.as_view(), name="profile-update"),
    path('accounts/', include('allauth.urls')),
    path('', include('wallofshameapp.urls')),
]
